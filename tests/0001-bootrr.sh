#!/bin/sh

source /usr/lib/ci-testsuite/utils/helpers.sh

command -v bootrr > /dev/null 2>&1

if [ $? -gt 0 ]; then
	testsuite_report_test_skipped $1
	exit 0
fi

# BOOTRR test

testsuite_report_start_case $1
bootrr
testsuite_report_end_case_status $1

exit 0
