install:
	install -d ${SYSTEMD_PREFIX}
	install -m 0644 ci-testsuite.service ${SYSTEMD_PREFIX}/

	install -d ${PREFIX}/lib/systemd/system-shutdown/
	install -m 0755 ci-testsuite-shutdown ${PREFIX}/lib/systemd/system-shutdown/

	install -d ${PREFIX}/bin
	install -m 0755 ci-testsuite.sh ${PREFIX}/bin/

	install -d ${PREFIX}/lib/ci-testsuite/
	install -d ${PREFIX}/lib/ci-testsuite/utils
	install -d ${PREFIX}/lib/ci-testsuite/tests

	install -m 0755 utils/helpers.sh ${PREFIX}/lib/ci-testsuite/utils/

	install -m 0755 tests/0000-smoke.sh ${PREFIX}/lib/ci-testsuite/tests/
	install -m 0755 tests/0001-bootrr.sh ${PREFIX}/lib/ci-testsuite/tests/

