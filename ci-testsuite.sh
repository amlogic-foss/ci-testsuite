#!/bin/sh

source /usr/lib/ci-testsuite/utils/helpers.sh

for arg in `cat /proc/cmdline`; do
	arg_name=`echo $arg | cut -d= -f1`
	arg_val=`echo $arg | cut -d= -f2`
	if [ "$arg_name" == "ci_testsuite_skip" ]; then
		ci_testsuite_skip="$ci_testsuite_skip `echo $arg_val | tr ',' ' '`"
	elif [ "$arg_name" == "ci_testsuite_allow" ]; then
		ci_testsuite_allow="$ci_testsuite_allow `echo $arg_val | tr ',' ' '`"
	fi
done

export ci_testsuite_skip=$ci_testsuite_skip
export ci_testsuite_allow=$ci_testsuite_allow

exec >> /dev/kmsg

# sleep for the default driver_deferred_probe_timeout delay
sleep 10

TESTS=`ls /usr/lib/ci-testsuite/tests/`

if [ -n "$TESTS" ]; then
	testsuite_report_start_case testsuite-loop
	START_TIME=`date +"%s"`
	for testscript in $TESTS; do
		TESTNAME=`basename $testscript .sh`

		# Check against the skip list
		if testsuite_check_skip $TESTNAME; then
			continue
		fi

		/usr/lib/ci-testsuite/tests/$testscript $TESTNAME
		if [ $? -gt 0 ]; then
			testsuite_report_test_failure $TESTNAME
		fi
	done
	END_TIME=`date +"%s"`
	testsuite_report_end_case testsuite-loop
	testsuite_report_test_data testsuite-loop `expr $END_TIME - $START_TIME` "seconds"
else
	testsuite_report_test_skipped testsuite-loop
fi

exit 0
